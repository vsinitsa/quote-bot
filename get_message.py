import discord
import time
import random
import os
import datetime
from pytz import timezone

class QuoteBot(discord.Client):
    def __init__(self):
        self.messages = None
        self.new_general = None
        discord.Client.__init__(self)

    async def on_ready(self):
        print('Logged in as {0.user}'.format(client))
        general = self.get_channel(109551664364548096)
        self.new_general = self.get_channel(594938027198119963)
        start = time.time()
        temp = await general.history(limit=None).flatten()
        self.messages = []
        print(time.time()-start)
        print(f"Retrieved: {len(temp)} messages")
        start = time.time()
        for m in temp:
            if m.content != "":
                self.messages.append(m)
        print(time.time()-start)
        print(len(self.messages))
        

    async def on_message(self, message):
        if message.author == self.user:
            return
        
        if message.content.startswith('/quote'):
            random_msg = random.choice(self.messages)
            embed = discord.Embed(title=f"{random_msg.author}", color=discord.Colour.dark_blue())
            if ".com" in random_msg.content or ".org" in random_msg.content:
                await self.new_general.send(random_msg.content)
            else:
                embed.add_field(name="Said, and I quote:", value=random_msg.content)
            embed.set_footer(text=f"At: {random_msg.created_at.astimezone(timezone('US/Pacific'))}")
            await self.new_general.send(embed=embed)

if __name__ == "__main__":
    try:
        client = QuoteBot()
        client.run(os.environ.get("TOKEN"))
    except KeyboardInterrupt as e:
        print("Shutting down")