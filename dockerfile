FROM python:3.7

ENV TOKEN default

WORKDIR /app
COPY get_message.py .
COPY requirements.txt .

RUN apt update
RUN pip3 install -r requirements.txt

CMD python3 -u get_message.py
